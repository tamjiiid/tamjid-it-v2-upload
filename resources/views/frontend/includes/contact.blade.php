<div class="section-title">
    <p>For inquiries or collaborations please contact me by email at <strong>@if ($about){{$about->email}}@endif</strong> or
        fill out the form below.</p>
</div>

<form method="post" action="{{route('contact.send')}}" name="sentMessage" id="contactForm" >
    {{csrf_field()}}
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" id="name" name="name" class="form-control" placeholder="Name" required="required">
                <p class="help-block text-danger"></p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <input type="email" id="email" name="email" class="form-control" placeholder="Email" required="required">
                <p class="help-block text-danger"></p>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <input type="text" id="subject" name="subject" class="form-control" placeholder="Subject" required="required">
                <p class="help-block text-danger"></p>
            </div>
        </div>
    </div>
    <div class="form-group">
                    <textarea name="message" id="message" class="form-control" rows="4" placeholder="Message"
                              required></textarea>
        <p class="help-block text-danger"></p>
    </div>
    <div id="success"></div>
    <button type="submit" class="btn btn-default btn-lg">Send Message</button>
</form>