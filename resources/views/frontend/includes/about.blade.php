<div class="section-title">
    <div class="row">
        @if ($about)
            <img src="{{url('uploads/user-info/'.$about->avatar)}}" class="img-responsive pull-left" alt="">
            {!! $about->description !!}
        @endif


        <div class="social">
            <ul>
                @if ($socials)
                    @foreach ($socials as $social)
                        <li><a href="{{$social->url}}" target="_blank"><i class="fa {{$social->fa_icon}}"></i></a></li>
                    @endforeach
                @else
                    <h4>No social link found</h4>
                @endif

            </ul>
        </div>
    </div>
</div>