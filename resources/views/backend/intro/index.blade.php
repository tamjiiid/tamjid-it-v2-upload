@extends('backend.layouts.main')

@section('title','Website Intro')

@push('css')
    <!-- JQuery DataTable Css -->
    <link href="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css')}}" rel="stylesheet">
@endpush

@section('content')
    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            WEBSITE INTRO
                        </h2>
                    </div>
                    <div class="body">
                        @if (isset($intro))
                            <h4>First Name</h4>
                            <p>- {{$intro->first_name}}</p>
                            <h4>Last Name</h4>
                            <p>- {{$intro->last_name}}</p>
                            <h4>Background Image</h4>
                            <p><img src="{{url('uploads/intro/'.$intro->background_image)}}" alt="{{config('app.name')}}" class="img-responsive" /></p>
                            <h4>Profession</h4>
                            <p>- {{$intro->profession}}</p>
                            <h4>Position</h4>
                            <p>- {{$intro->position}}</p>
                            <a href="{{route('intro.edit', $intro->id)}}" type="button" class="btn btn-warning m-t-15 waves-effect">EDIT</a>
                        @else
                            <h3>No data found</h3>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{asset('assets/backend/plugins/jquery-datatable/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.flash.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/jszip.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/backend/plugins/jquery-datatable/extensions/export/buttons.print.min.js')}}"></script>

@endpush