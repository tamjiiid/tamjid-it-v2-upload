<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Intro extends Model
{
    protected $fillable = ['first_name', 'last_name', 'background_image', 'profession', 'position'];
}
