<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Intro;
use App\Models\Project;
use App\Models\SocialLink;
use App\Models\UserInfo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomepageController extends Controller
{
    public function index(){
        $intro = Intro::all()->first();
        $about = UserInfo::all()->first();
        $categories = Category::inRandomOrder()->get();
        $socials = SocialLink::all();
        $projects = Project::inRandomOrder()->get();
        return view('frontend.index',compact('intro','about','categories','projects','socials'));
    }
}
