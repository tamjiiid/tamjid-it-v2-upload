<?php

namespace App\Http\Controllers\Backend;

use App\Models\UserInfo;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class UserInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info = UserInfo::all()->first();
        return view('backend.userinfo.index',compact('info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.userinfo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'avatar' => 'required',
            'description' => 'required',
            'email' => 'required'
        ]);

        $image = $request->file('avatar');
        $slug = str_slug(config('app.name'));

        if (isset($image)) {
//            make unique name image
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('uploads/user-info')) {
                Storage::disk('public')->makeDirectory('uploads/user-info');
            }
            $resizeImage = Image::make($image)->resize(200, 199)->save();
            Storage::disk('public')->put('uploads/user-info/' . $imageName, $resizeImage);
        } else {
            $imageName = 'default.png';
        }
        $info = new UserInfo();

        $info->avatar = $imageName;
        $info->email = $request->email;
        $info->description = $request->description;

        $info->save();

        Toastr::success('User Info saved successfully!', 'Done');

        return redirect()->route('user-info.index')->with('successMsg', 'User Info successfully saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
