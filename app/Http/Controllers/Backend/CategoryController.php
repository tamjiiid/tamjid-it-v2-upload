<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\Project;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('backend.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:20|min:3',
        ]);

        $category = new Category;
        $category->name = $request->name;
        $category->save();

        Toastr::success('Category saved successfully!', 'Done');

        return redirect()->route('category.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);
        return view('backend.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('backend.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:20|min:3',
        ]);

        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->update();
        Toastr::success('Category updated successfully!', 'Done');

        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);

        $projects = Project::where('category_id', $id);

        foreach ($projects as $project)
            if (Storage::disk('public')->exists('uploads/project/' . $project->image)) {
                Storage::disk('public')->delete('uploads/project/' . $project->image);
                $project->delete();
            }

//        foreach ($projects as $project) {
//            if (File::exists('uploads/project/' . $project->image)) {
//            dd(File::exists('/uploads/project/' . $project->image));
//                File::delete('uploads/project/' . $project->image);
//            }
//        }
//        $projects->delete();
        $category->delete();

        Toastr::success('Category deleted successfully!', 'Done');
        return redirect()->back();
    }
}
