<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\Project;
use Brian2694\Toastr\Facades\Toastr;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
//        $categories = Category::all();
        return view('backend.project.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('backend.project.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'name' => 'required|min:3|max:20',
            'url' => 'required',
            'image' => 'required|image',
        ]);

        $image = $request->file('image');
        $slug = str_slug($request->name);

        if (isset($image)) {
//            make unique name image
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('uploads/project')) {
                Storage::disk('public')->makeDirectory('uploads/project');
            }
            $resizeImage = Image::make($image)->resize(400, 267)->save();
            Storage::disk('public')->put('uploads/project/' . $imageName, $resizeImage);
        } else {
            $imageName = 'default.png';
        }

        $project = new Project();

        $project->name = $request->name;
        $project->category_id = $request->category_id;
        $project->description = $request->description;
        $project->url = $request->url;
        $project->image = $imageName;

        $project->save();

        Toastr::success('Project saved successfully!', 'Done');

        return redirect()->route('project.index')->with('successMsg', 'Project successfully saved.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::findOrFail($id);
        $categories = Category::all();
        return view('backend.project.edit', compact('project', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'name' => 'required|min:3|max:20',
            'url' => 'required',
            'image' => 'required|image',
        ]);

        $image = $request->file('image');
        $slug = str_slug($request->name);

        if (isset($image)) {
//            make unique name image
            $currentDate = Carbon::now()->toDateString();
            $imageName = $slug . '-' . $currentDate . '' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!Storage::disk('public')->exists('uploads/project')) {
                Storage::disk('public')->makeDirectory('uploads/project');
            }
            $resizeImage = Image::make($image)->resize(400, 267)->save();
            Storage::disk('public')->put('uploads/project/' . $imageName, $resizeImage);
        } else {
            $imageName = 'default.png';
        }

        $project = Project::findOrFail($id);

        $project->name = $request->name;
        $project->category_id = $request->category_id;
        $project->description = $request->description;
        $project->url = $request->url;
        $project->image = $imageName;

        $project->update();

        Toastr::success('Project updated successfully!', 'Done');

        return redirect()->route('project.index')->with('successMsg', 'Project successfully updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd($id);
    }
}
